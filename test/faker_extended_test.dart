import 'dart:math' as $math;
import 'package:faker/faker.dart' as $orig;
import 'package:faker_extended/faker_extended.dart';
import 'package:fixnum/fixnum.dart';
import 'package:test/test.dart';


void main() {
  group('number generators', () {
    group('int64', () {
      List<Int64> _generate(Int64 Function() callback, {int iterations = 1000}) {
        List<Int64> actuals = [];

        for(var i = 0; i < iterations; i++) {
          actuals.add(callback());
        }

        return actuals;
      }

      test('basic', () {
        var actuals = _generate(() => faker.random.int64());

        expect(actuals, everyElement(isA<Int64>()),                             reason: 'Returned value of unexpected type'                                   );
        expect(actuals, anyElement(greaterThan(Int32.MAX_VALUE.toInt())),       reason: 'Generated numbers did not include any 64-bit values, but should have');
        // expect(actuals, anyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt())), reason: 'Generated numbers did not include any 32-bit values, but should have'); // this will simply take far too many iterations to prove
      });

      group('with min', () {
        int expectedMin = 0;

        setUp(() {
          expectedMin = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());
        });

        test('32-bit', () {
          var actuals = _generate(() => faker.randomGenerator.int64(min: expectedMin));

          expect(actuals, everyElement(isA<Int64>()),                       reason: 'Returned value of unexpected type'                                         );
          expect(actuals, everyElement(greaterThan(expectedMin)),           reason: 'Generated numbers unexpectedly include value(s) less than expected minimum');
          expect(actuals, anyElement(greaterThan(Int32.MAX_VALUE.toInt())), reason: 'Generated numbers did not include any 64-bit values, but should have'      );
        });

        test('64-bit', () {
          expectedMin = expectedMin << 32;
          var actuals = _generate(() => faker.randomGenerator.int64(min: expectedMin));

          expect(actuals, everyElement(isA<Int64>()),                       reason: 'Returned value of unexpected type'                                         );
          expect(actuals, everyElement(greaterThan(expectedMin)),           reason: 'Generated numbers unexpectedly include value(s) less than expected minimum');
          expect(actuals, anyElement(greaterThan(Int32.MAX_VALUE.toInt())), reason: 'Generated numbers did not include any 64-bit values, but should have'      );
        });
      });

      group('with max', () {
        int expectedMax = 0;

        setUp(() {
          expectedMax = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());
        });

        test('32-bit', () {
          var actuals = _generate(() => faker.randomGenerator.int64(max: expectedMax));

          expect(actuals, everyElement(isA<Int64>()),                      reason: 'Returned value of unexpected type'                       );
          expect(actuals, everyElement(lessThan(Int32.MAX_VALUE.toInt())), reason: 'Generated values unexpectedly included non-32-bit values');
        });

        test('64-bit', () {
          expectedMax = expectedMax << 32;
          var actuals = _generate(() => faker.randomGenerator.int64(max: expectedMax));

          expect(actuals, everyElement(isA<Int64>()),                       reason: 'Returned value of unexpected type'                                            );
          expect(actuals, everyElement(lessThan(expectedMax)),              reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
          expect(actuals, anyElement(greaterThan(Int32.MAX_VALUE.toInt())), reason: 'Generated numbers did not include any 64-bit values, but should have'         );
        });
      });

      group('with min + max', () {
        int expectedMin = 0;
        int expectedMax = 0;

        setUp(() {
          expectedMin = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());
          expectedMax = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());

          if (expectedMin > expectedMax) {
            var temp = expectedMin;
            expectedMin = expectedMax;
            expectedMax = temp;
          }
        });

        group('32-bit min', () {
          test('32-bit max', () {
            var actuals = _generate(() => faker.randomGenerator.int64(min: expectedMin, max: expectedMax));

            expect(actuals, everyElement(isA<Int64>()),             reason: 'Returned value of unexpected type'                                            );
            expect(actuals, everyElement(lessThan(expectedMax)),    reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
            expect(actuals, everyElement(greaterThan(expectedMin)), reason: 'Generated numbers unexpectedly include value(s) less than expected minimum'   );
          });

          test('64-bit max', () {
            expectedMax = expectedMax << 32;
            var actuals = _generate(() => faker.randomGenerator.int64(min: expectedMin, max: expectedMax));

            expect(actuals, everyElement(isA<Int64>()),                       reason: 'Returned value of unexpected type'                                            );
            expect(actuals, everyElement(lessThan(expectedMax)),              reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
            expect(actuals, everyElement(greaterThan(expectedMin)), reason: 'Generated numbers unexpectedly include value(s) less than expected minimum'   );
            expect(actuals, anyElement(greaterThan(Int32.MAX_VALUE.toInt())), reason: 'Generated numbers did not include any 64-bit values, but should have'         );
          });
        });

        group('64-bit min', () {
          setUp(() {
            expectedMin = expectedMin << 32;
          });

          test('32-bit max', () {
            var callback = () => _generate(() => faker.randomGenerator.int64(min: expectedMin, max: expectedMax));

            expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('must be less than value passed for `max`'))), reason: 'Expected error not thrown');
          });

          test('64-bit max', () {
            expectedMax = expectedMax << 32;
            var actuals = _generate(() => faker.randomGenerator.int64(min: expectedMin, max: expectedMax));

            expect(actuals, everyElement(isA<Int64>()),                       reason: 'Returned value of unexpected type'                                            );
            expect(actuals, everyElement(lessThan(expectedMax)),              reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
            expect(actuals, everyElement(greaterThan(expectedMin)),           reason: 'Generated numbers unexpectedly include value(s) less than expected minimum'   );
            expect(actuals, anyElement(greaterThan(Int32.MAX_VALUE.toInt())), reason: 'Generated numbers did not include any 64-bit values, but should have'         );
          });
        });

        test('min > max', () {
          var callback = () => _generate(() => faker.randomGenerator.int64(min: expectedMax, max: expectedMin));

          expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('must be less than value passed for `max`'))), reason: 'Expected error not thrown');
        });
      });
    });

    group('int32', () {
      List<Int32> _generate(Int32 Function() callback, {int iterations = 1000}) {
        List<Int32> actuals = [];

        for(var i = 0; i < iterations; i++) {
          actuals.add(callback());
        }

        return actuals;
      }

      test('basic', () {
        var actuals = _generate(() => faker.randomGenerator.int32());

        expect(actuals, everyElement(isA<Int32>()),                               reason: 'Returned value of unexpected type'                                   );
        expect(actuals, everyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt())), reason: 'Generated numbers did not include any 32-bit values, but should have'); // this will simply take far too many iterations to prove
      });

      group('with min', () {
        int expectedMin = 0;

        setUp(() {
          expectedMin = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());
        });

        test('32-bit', () {
          var actuals = _generate(() => faker.randomGenerator.int32(min: expectedMin));

          expect(actuals, everyElement(isA<Int32>()),                       reason: 'Returned value of unexpected type'                                         );
          expect(actuals, everyElement(greaterThan(expectedMin)),           reason: 'Generated numbers unexpectedly include value(s) less than expected minimum');
        });

        test('64-bit', () {
          expectedMin = expectedMin << 32;
          var callback = () => _generate(() => faker.randomGenerator.int32(min: expectedMin));

          expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('must be a 32-bit number'))), reason: 'Expected error not thrown');
        });
      });

      group('with max', () {
        int expectedMax = 0;

        setUp(() {
          expectedMax = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());
        });

        test('32-bit', () {
          var actuals = _generate(() => faker.randomGenerator.int32(max: expectedMax));

          expect(actuals, everyElement(isA<Int32>()),                       reason: 'Returned value of unexpected type'                                            );
          expect(actuals, everyElement(lessThan(expectedMax)),              reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
        });

        test('64-bit', () {
          expectedMax = expectedMax << 32;
          var actuals = _generate(() => faker.randomGenerator.int32(max: expectedMax));

          expect(actuals, everyElement(isA<Int32>()),                       reason: 'Returned value of unexpected type'                                            );
          expect(actuals, everyElement(lessThan(expectedMax)),              reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
        });
      });

      group('with min + max', () {
        int expectedMin = 0;
        int expectedMax = 0;

        setUp(() {
          expectedMin = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());
          expectedMax = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());

          if (expectedMin > expectedMax) {
            var temp = expectedMin;
            expectedMin = expectedMax;
            expectedMax = temp;
          }
        });

        group('32-bit min', () {
          test('32-bit max', () {
            var actuals = _generate(() => faker.randomGenerator.int32(min: expectedMin, max: expectedMax));

            expect(actuals, everyElement(isA<Int32>()),             reason: 'Returned value of unexpected type'                                            );
            expect(actuals, everyElement(lessThan(expectedMax)),    reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
            expect(actuals, everyElement(greaterThan(expectedMin)), reason: 'Generated numbers unexpectedly include value(s) less than expected minimum'   );
          });

          test('64-bit max', () {
            expectedMax = expectedMax << 32;
            var actuals = _generate(() => faker.randomGenerator.int32(min: expectedMin, max: expectedMax));

            expect(actuals, everyElement(isA<Int32>()),                       reason: 'Returned value of unexpected type'                                            );
            expect(actuals, everyElement(lessThan(expectedMax)),              reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
            expect(actuals, everyElement(greaterThan(expectedMin)), reason: 'Generated numbers unexpectedly include value(s) less than expected minimum'   );
          });
        });

        group('64-bit min', () {
          setUp(() {
            expectedMin = expectedMin << 32;
          });

          test('32-bit max', () {
            var callback = () => _generate(() => faker.randomGenerator.int32(min: expectedMin, max: expectedMax));

            expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('must be a 32-bit number'))), reason: 'Expected error not thrown');
          });

          test('64-bit max', () {
            expectedMax = expectedMax << 32;
            var callback = () => _generate(() => faker.randomGenerator.int32(min: expectedMin, max: expectedMax));

            expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('must be a 32-bit number'))), reason: 'Expected error not thrown');
          });
        });

        test('min > max', () {
          var callback = () =>
            _generate(() =>
              faker.randomGenerator.int32(min: expectedMax, max: expectedMin));

          expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('must be less than value passed for `max`'))), reason: 'Expected error not thrown');
        });
      });
    });

    group('int64s', () {
      List<List<Int64>> _generate(List<Int64> Function() callback, {int iterations = 1000}) {
        List<List<Int64>> actuals = [];

        for(var i = 0; i < iterations; i++) {
          actuals.add(callback());
        }

        return actuals;
      }

      int expectedCount = 0;

      setUp(() {
        expectedCount = $orig.faker.randomGenerator.integer(1000, min: 10);
      });

      test('basic', () {
        var actuals = _generate(() => faker.randomGenerator.int64s(expectedCount));

        expect(actuals, everyElement(everyElement(isA<Int64>())),                       reason: 'generated list unexpectedly contains elements of unexpected type' );
        expect(actuals, everyElement(hasLength(expectedCount)),                         reason: 'generated list unexpectedly has more/less elements than requested');
        expect(actuals, everyElement(anyElement(greaterThan(Int32.MAX_VALUE.toInt()))), reason: 'generated list did not include any 64-bit values, but should have');
        // expect(actuals, everyElement(anyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt()))), reason: 'Generated numbers did not include any 32-bit values, but should have'); // this will simply take far too many iterations to prove
      });

      group('with min', () {
        int expectedMin = 0;

        setUp(() {
          expectedMin = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());
        });

        test('32-bit', () {
          var actuals = _generate(() => faker.randomGenerator.int64s(expectedCount, min: expectedMin));

          expect(actuals, everyElement(everyElement(isA<Int64>())),                       reason: 'generated list unexpectedly contains elements of unexpected type'        );
          expect(actuals, everyElement(hasLength(expectedCount)),                         reason: 'generated list unexpectedly has more/less elements than requested'       );
          expect(actuals, everyElement(anyElement(greaterThan(Int32.MAX_VALUE.toInt()))), reason: 'generated list did not include any 64-bit values, but should have'       );
          expect(actuals, everyElement(everyElement(greaterThan(expectedMin))),           reason: 'generated list unexpectedly included value(s) less than expected minimum');
        });

        test('64-bit', () {
          expectedMin = expectedMin << 32;
          var actuals = _generate(() => faker.randomGenerator.int64s(expectedCount, min: expectedMin));

          expect(actuals, everyElement(everyElement(isA<Int64>())),                       reason: 'generated list unexpectedly contains elements of unexpected type'       );
          expect(actuals, everyElement(hasLength(expectedCount)),                         reason: 'generated list unexpectedly has more/less elements than requested'      );
          expect(actuals, everyElement(anyElement(greaterThan(Int32.MAX_VALUE.toInt()))), reason: 'generated list did not include any 64-bit values, but should have'      );
          expect(actuals, everyElement(everyElement(greaterThan(expectedMin))),           reason: 'generated list unexpectedly include value(s) less than expected minimum');
        });
      });

      group('with max', () {
        int expectedMax = 0;

        setUp(() {
          expectedMax = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());
        });

        test('32-bit', () {
          var actuals = _generate(() => faker.randomGenerator.int64s(expectedCount, max: expectedMax));

          expect(actuals, everyElement(everyElement(isA<Int64>())),                       reason: 'generated list unexpectedly contains elements of unexpected type' );
          expect(actuals, everyElement(hasLength(expectedCount)),                         reason: 'generated list unexpectedly has more/less elements than requested');
          expect(actuals, everyElement(everyElement(lessThan(Int32.MAX_VALUE.toInt()))),  reason: 'Generated values unexpectedly included non-32-bit values'         );
        });

        test('64-bit', () {
          expectedMax = expectedMax << 32;
          var actuals = _generate(() => faker.randomGenerator.int64s(expectedCount, max: expectedMax));

          expect(actuals, everyElement(everyElement(isA<Int64>())),                       reason: 'generated list unexpectedly contains elements of unexpected type'          );
          expect(actuals, everyElement(hasLength(expectedCount)),                         reason: 'generated list unexpectedly has more/less elements than requested'         );
          expect(actuals, everyElement(anyElement(greaterThan(Int32.MAX_VALUE.toInt()))), reason: 'generated list did not include any 64-bit values, but should have'         );
          expect(actuals, everyElement(everyElement(lessThan(expectedMax))),              reason: 'generated list unexpectedly include value(s) greater than expected maximum');
        });
      });

      group('with min + max', () {
        int expectedMin = 0;
        int expectedMax = 0;

        setUp(() {
          expectedMin = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());
          expectedMax = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());

          if (expectedMin > expectedMax) {
            var temp = expectedMin;
            expectedMin = expectedMax;
            expectedMax = temp;
          }
        });

        group('32-bit min', () {
          test('32-bit max', () {
            var actuals = _generate(() => faker.randomGenerator.int64s(expectedCount, min: expectedMin, max: expectedMax));

            expect(actuals, everyElement(everyElement(isA<Int64>())),                       reason: 'generated list unexpectedly contains elements of unexpected type'          );
            expect(actuals, everyElement(hasLength(expectedCount)),                         reason: 'generated list unexpectedly has more/less elements than requested'         );
            expect(actuals, everyElement(everyElement(lessThan(expectedMax))),              reason: 'generated list unexpectedly include value(s) greater than expected maximum');
            expect(actuals, everyElement(everyElement(greaterThan(expectedMin))),           reason: 'generated list unexpectedly include value(s) less than expected minimum'   );
          });

          test('64-bit max', () {
            expectedMax = expectedMax << 32;
            var actuals = _generate(() => faker.randomGenerator.int64s(expectedCount, min: expectedMin, max: expectedMax));

            expect(actuals, everyElement(everyElement(isA<Int64>())),                       reason: 'generated list unexpectedly contains elements of unexpected type'          );
            expect(actuals, everyElement(hasLength(expectedCount)),                         reason: 'generated list unexpectedly has more/less elements than requested'         );
            expect(actuals, everyElement(anyElement(greaterThan(Int32.MAX_VALUE.toInt()))), reason: 'generated list did not include any 64-bit values, but should have'         );
            expect(actuals, everyElement(everyElement(lessThan(expectedMax))),              reason: 'generated list unexpectedly include value(s) greater than expected maximum');
            expect(actuals, everyElement(everyElement(greaterThan(expectedMin))),           reason: 'generated list unexpectedly include value(s) less than expected minimum'   );
          });
        });

        group('64-bit min', () {
          setUp(() {
            expectedMin = expectedMin << 32;
          });

          test('32-bit max', () {
            var callback = () => _generate(() => faker.randomGenerator.int64s(expectedCount, min: expectedMin, max: expectedMax));

            expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('must be less than value passed for `max`'))), reason: 'Expected error not thrown');
          });

          test('64-bit max', () {
            expectedMax = expectedMax << 32;
            var actuals = _generate(() => faker.randomGenerator.int64s(expectedCount, min: expectedMin, max: expectedMax));

            expect(actuals, everyElement(everyElement(isA<Int64>())),                       reason: 'generated list unexpectedly contains elements of unexpected type'          );
            expect(actuals, everyElement(hasLength(expectedCount)),                         reason: 'generated list unexpectedly has more/less elements than requested'         );
            expect(actuals, everyElement(anyElement(greaterThan(Int32.MAX_VALUE.toInt()))), reason: 'generated list did not include any 64-bit values, but should have'         );
            expect(actuals, everyElement(everyElement(lessThan(expectedMax))),              reason: 'generated list unexpectedly include value(s) greater than expected maximum');
            expect(actuals, everyElement(everyElement(greaterThan(expectedMin))),           reason: 'generated list unexpectedly include value(s) less than expected minimum'   );
          });
        });

        test('min > max', () {
          var callback = () => _generate(() => faker.randomGenerator.int64s(expectedCount, min: expectedMax, max: expectedMin));

          expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('must be less than value passed for `max`'))), reason: 'Expected error not thrown');
        });
      });
    });

    group('int32s', () {
      List<List<Int32>> _generate(List<Int32> Function() callback, {int iterations = 1000}) {
        List<List<Int32>> actuals = [];

        for(var i = 0; i < iterations; i++) {
          actuals.add(callback());
        }

        return actuals;
      }

      int expectedCount = 0;

      setUp(() {
        expectedCount = $orig.faker.randomGenerator.integer(1000, min: 10);
      });

      test('basic', () {
        var actuals = _generate(() => faker.randomGenerator.int32s(expectedCount));

        expect(actuals, everyElement(everyElement(isA<Int32>())),                             reason: 'generated list unexpectedly contains elements of unexpected type' );
        expect(actuals, everyElement(hasLength(expectedCount)),                               reason: 'generated list unexpectedly has more/less elements than requested');
        expect(actuals, everyElement(anyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt()))), reason: 'generated list unexpectedly included non-32-bit value(s)'         );
      });

      group('with min', () {
        int expectedMin = 0;

        setUp(() {
          expectedMin = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());
        });

        test('32-bit', () {
          var actuals = _generate(() => faker.randomGenerator.int32s(expectedCount, min: expectedMin));

          expect(actuals, everyElement(everyElement(isA<Int32>())),                       reason: 'generated list unexpectedly contains elements of unexpected type'        );
          expect(actuals, everyElement(hasLength(expectedCount)),                         reason: 'generated list unexpectedly has more/less elements than requested'       );
          expect(actuals, everyElement(anyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt()))), reason: 'generated list unexpectedly included non-32-bit value(s)'       );
          expect(actuals, everyElement(everyElement(greaterThan(expectedMin))),           reason: 'generated list unexpectedly included value(s) less than expected minimum');
        });

        test('64-bit', () {
          expectedMin = expectedMin << 32;
          var callback = () => _generate(() => faker.randomGenerator.int32s(expectedCount, min: expectedMin));

          expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('`min` must be a 32-bit number'))), reason: 'Expected error not thrown');
        });
      });

      group('with max', () {
        int expectedMax = 0;

        setUp(() {
          expectedMax = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());
        });

        test('32-bit', () {
          var actuals = _generate(() => faker.randomGenerator.int32s(expectedCount, max: expectedMax));

          expect(actuals, everyElement(everyElement(isA<Int32>())),                       reason: 'generated list unexpectedly contains elements of unexpected type' );
          expect(actuals, everyElement(hasLength(expectedCount)),                         reason: 'generated list unexpectedly has more/less elements than requested');
          expect(actuals, everyElement(everyElement(lessThan(Int32.MAX_VALUE.toInt()))),  reason: 'Generated values unexpectedly included non-32-bit values'         );
        });

        test('64-bit', () {
          expectedMax = expectedMax << 32;
          var actuals = _generate(() => faker.randomGenerator.int32s(expectedCount, max: expectedMax));

          expect(actuals, everyElement(everyElement(isA<Int32>())),                       reason: 'generated list unexpectedly contains elements of unexpected type'          );
          expect(actuals, everyElement(hasLength(expectedCount)),                         reason: 'generated list unexpectedly has more/less elements than requested'         );
          expect(actuals, everyElement(anyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt()))), reason: 'generated list unexpectedly included non-32-bit value(s)'         );
          expect(actuals, everyElement(everyElement(lessThan(expectedMax))),              reason: 'generated list unexpectedly include value(s) greater than expected maximum');
        });
      });

      group('with min + max', () {
        int expectedMin = 0;
        int expectedMax = 0;

        setUp(() {
          expectedMin = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());
          expectedMax = $orig.faker.randomGenerator.integer(Int32.MAX_VALUE.toInt());

          if (expectedMin > expectedMax) {
            var temp = expectedMin;
            expectedMin = expectedMax;
            expectedMax = temp;
          }
        });

        group('32-bit min', () {
          test('32-bit max', () {
            var actuals = _generate(() => faker.randomGenerator.int32s(expectedCount, min: expectedMin, max: expectedMax));

            expect(actuals, everyElement(everyElement(isA<Int32>())),                       reason: 'generated list unexpectedly contains elements of unexpected type'          );
            expect(actuals, everyElement(hasLength(expectedCount)),                         reason: 'generated list unexpectedly has more/less elements than requested'         );
            expect(actuals, everyElement(everyElement(lessThan(expectedMax))),              reason: 'generated list unexpectedly include value(s) greater than expected maximum');
            expect(actuals, everyElement(everyElement(greaterThan(expectedMin))),           reason: 'generated list unexpectedly include value(s) less than expected minimum'   );
          });

          test('64-bit max', () {
            expectedMax = expectedMax << 32;
            var actuals = _generate(() => faker.randomGenerator.int32s(expectedCount, min: expectedMin, max: expectedMax));

            expect(actuals, everyElement(everyElement(isA<Int32>())),                       reason: 'generated list unexpectedly contains elements of unexpected type'          );
            expect(actuals, everyElement(hasLength(expectedCount)),                         reason: 'generated list unexpectedly has more/less elements than requested'         );
            expect(actuals, everyElement(anyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt()))), reason: 'generated list unexpectedly included non-32-bit value(s)'         );
            expect(actuals, everyElement(everyElement(lessThan(expectedMax))),              reason: 'generated list unexpectedly include value(s) greater than expected maximum');
            expect(actuals, everyElement(everyElement(greaterThan(expectedMin))),           reason: 'generated list unexpectedly include value(s) less than expected minimum'   );
          });
        });

        group('64-bit min', () {
          setUp(() {
            expectedMin = expectedMin << 32;
          });

          test('32-bit max', () {
            var callback = () => _generate(() => faker.randomGenerator.int32s(expectedCount, min: expectedMin, max: expectedMax));

            expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('`min` must be a 32-bit number'))), reason: 'Expected error not thrown');
          });

          test('64-bit max', () {
            expectedMax = expectedMax << 32;
            var callback = () => _generate(() => faker.randomGenerator.int32s(expectedCount, min: expectedMin, max: expectedMax));

            expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('`min` must be a 32-bit number'))), reason: 'Expected error not thrown');
          });
        });

        test('min > max', () {
          var callback = () => _generate(() => faker.randomGenerator.int32s(expectedCount, min: expectedMax, max: expectedMin));

          expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('must be less than value passed for `max`'))), reason: 'Expected error not thrown');
        });
      });
    });

    // @depends(['int64', 'int32'])
    group('ints()', () {
      int expectedCount = 0;

      setUp(() {
        expectedCount = $orig.faker.randomGenerator.integer(1000, min: 10);
      });

      group('basic', () {
        test('mixed', () {
          var actual = faker.randomGenerator.ints(expectedCount);

          expect(actual, hasLength(equals(expectedCount)),                       reason: 'Generated list has unexpected number of elements'                    );
          expect(actual, anyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt())), reason: 'Generated numbers did not include any 32-bit values, but should have');
          expect(actual, anyElement(greaterThan(Int32.MAX_VALUE.toInt())),       reason: 'Generated numbers did not include any 64-bit values, but should have');
        });

        test('only32bit', () {
          var actual = faker.randomGenerator.ints(expectedCount, only32bit: true);

          expect(actual, hasLength(equals(expectedCount)),                         reason: 'Generated list has unexpected number of elements' );
          expect(actual, everyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt())), reason: 'Generated values unexpectedly included non-32-bit values');
        });

        test('only64bit', () {
          var actual = faker.randomGenerator.ints(expectedCount, only64bit: true);

          expect(actual, hasLength(equals(expectedCount)),                   reason: 'Generated list has unexpected number of elements' );
          expect(actual, everyElement(greaterThan(Int32.MAX_VALUE.toInt())), reason: 'Generated values unexpectedly included non-64-bit values');
        });
      });

      group('with min', () {
        int expectedMin = 0;

        setUp(() {
          expectedMin = $orig.faker.randomGenerator.integer(100, min: 10);
        });

        test('mixed', () {
          var actual = faker.randomGenerator.ints(expectedCount, min: expectedMin);

          expect(actual, hasLength(equals(expectedCount)),                       reason: 'Generated list has unexpected number of elements'                          );
          expect(actual, anyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt())), reason: 'Generated numbers did not include any 32-bit values, but should have'      );
          expect(actual, anyElement(greaterThan(Int32.MAX_VALUE.toInt())),       reason: 'Generated numbers did not include any 64-bit values, but should have'      );
          expect(actual, everyElement(greaterThanOrEqualTo(expectedMin)),        reason: 'Generated numbers unexpectedly include value(s) less than expected minimum');
        });

        group('only32bit', () {
          test('32-bit min', () {
            var actual = faker.randomGenerator.ints(expectedCount, min: expectedMin, only32bit: true);

            expect(actual, hasLength(equals(expectedCount)),                         reason: 'Generated list has unexpected number of elements'                          );
            expect(actual, everyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt())), reason: 'Generated values unexpectedly included non-32-bit values'                  );
            expect(actual, everyElement(greaterThanOrEqualTo(expectedMin)),          reason: 'Generated numbers unexpectedly include value(s) less than expected minimum');
          });

          test('64-bit min', () {
            expectedMin  = expectedMin << 32;
            var callback = () => faker.randomGenerator.ints(expectedCount, min: expectedMin, only32bit: true);

            expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('value of `min` must be a 32-bit number'))), reason: 'Expected error not thrown');
          });
        });

        group('only64bit', () {
          test('32-bit min', () {
            var actual = faker.randomGenerator.ints(expectedCount, min: expectedMin, only64bit: true);

            expect(actual, hasLength(equals(expectedCount)),                   reason: 'Generated list has unexpected number of elements'                          );
            expect(actual, everyElement(greaterThan(Int32.MAX_VALUE.toInt())), reason: 'Generated values unexpectedly included non-64-bit values'                  );
            expect(actual, everyElement(greaterThanOrEqualTo(expectedMin)),    reason: 'Generated numbers unexpectedly include value(s) less than expected minimum');
          });

          test('64-bit min', () {
            expectedMin = expectedMin << 32;
            var actual  = faker.randomGenerator.ints(expectedCount, min: expectedMin, only64bit: true);

            expect(actual, hasLength(equals(expectedCount)),                   reason: 'Generated list has unexpected number of elements'                          );
            expect(actual, everyElement(greaterThan(Int32.MAX_VALUE.toInt())), reason: 'Generated values unexpectedly included non-64-bit values'                  );
            expect(actual, everyElement(greaterThanOrEqualTo(expectedMin)),    reason: 'Generated numbers unexpectedly include value(s) less than expected minimum');
          });
        });
      });

      group('with max', () {
        int expectedMax = 0;

        setUp(() {
          expectedMax = $orig.faker.randomGenerator.integer(100, min: 10);
        });

        group('mixed', () {
          test('32-bit max', () {
            var actual = faker.randomGenerator.ints(expectedCount, max: expectedMax);

            expect(actual, hasLength(equals(expectedCount)),                       reason: 'Generated list has unexpected number of elements'                             );
            expect(actual, anyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt())), reason: 'Generated numbers did not include any 32-bit values, but should have'         );
            expect(actual, everyElement(lessThanOrEqualTo(expectedMax)),           reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
          });

          test('64-bit max', () {
            expectedMax = expectedMax << 32;
            var actual  = faker.randomGenerator.ints(expectedCount, max: expectedMax);

            expect(actual, hasLength(equals(expectedCount)),                       reason: 'Generated list has unexpected number of elements'                             );
            expect(actual, anyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt())), reason: 'Generated numbers did not include any 32-bit values, but should have'         );
            expect(actual, anyElement(greaterThan(Int32.MAX_VALUE.toInt())),       reason: 'Generated numbers did not include any 64-bit values, but should have'         );
            expect(actual, everyElement(lessThanOrEqualTo(expectedMax)),           reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
          });
        });

        group('only32bit', () {
          test('32-bit max', () {
            var actual = faker.randomGenerator.ints(expectedCount, max: expectedMax, only32bit: true);

            expect(actual, hasLength(equals(expectedCount)),                         reason: 'Generated list has unexpected number of elements'                             );
            expect(actual, everyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt())), reason: 'Generated values unexpectedly included non-32-bit values'                     );
            expect(actual, everyElement(lessThanOrEqualTo(expectedMax)),             reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
          });

          test('64-bit max', () {
            expectedMax = expectedMax << 32;
            var actual  = faker.randomGenerator.ints(expectedCount, max: expectedMax, only32bit: true);

            expect(actual, hasLength(equals(expectedCount)),                         reason: 'Generated list has unexpected number of elements'                             );
            expect(actual, everyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt())), reason: 'Generated values unexpectedly included non-32-bit values'                     );
            expect(actual, everyElement(lessThanOrEqualTo(expectedMax)),             reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
          });
        });

        group('only64bit', () {
          test('32-bit max', () {
            var callback = () => faker.randomGenerator.ints(expectedCount, max: expectedMax, only64bit: true);

            expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('value of `max` must be a 64-bit number'))), reason: 'Expected error not thrown');
          });
        });
      });

      group('with min + max', () {
        int expectedMin = 0;
        int expectedMax = 0;

        setUp(() {
          expectedMin = $orig.faker.randomGenerator.integer(100, min: 10);
          expectedMax = $orig.faker.randomGenerator.integer(100, min: 10);

          if (expectedMin > expectedMax) {
            var temp = expectedMin;
            expectedMin = expectedMax;
            expectedMax = temp;
          }
        });

        test('mixed', () {
          expectedMax = expectedMax << 32;
          var actual  = faker.randomGenerator.ints(expectedCount, min: expectedMin, max: expectedMax);

          expect(actual, hasLength(equals(expectedCount)),                       reason: 'Generated list has unexpected number of elements'                          );
          expect(actual, anyElement(lessThanOrEqualTo(Int32.MAX_VALUE.toInt())), reason: 'Generated numbers did not include any 32-bit values, but should have'      );
          expect(actual, anyElement(greaterThan(Int32.MAX_VALUE.toInt())),       reason: 'Generated numbers did not include any 64-bit values, but should have'      );
          expect(actual, everyElement(greaterThanOrEqualTo(expectedMin)),        reason: 'Generated numbers unexpectedly include value(s) less than expected minimum');
        });

        group('only32bit', () {
          group('32-bit min', () {
            test('32-bit max', () {
              var actual = faker.randomGenerator.ints(expectedCount, min: expectedMin, max: expectedMax, only32bit: true);

              expect(actual, hasLength(equals(expectedCount)),                reason: 'Generated list has unexpected number of elements'                             );
              expect(actual, everyElement(lessThanOrEqualTo(expectedMax)),    reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
              expect(actual, everyElement(greaterThanOrEqualTo(expectedMin)), reason: 'Generated numbers unexpectedly include value(s) less than expected minimum'   );
            });

            test('64-bit max', () {
              expectedMax = expectedMax << 32;
              var actual  = faker.randomGenerator.ints(expectedCount, min: expectedMin, max: expectedMax, only32bit: true);

              expect(actual, hasLength(equals(expectedCount)),                reason: 'Generated list has unexpected number of elements'                             );
              expect(actual, everyElement(lessThanOrEqualTo(expectedMax)),    reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
              expect(actual, everyElement(greaterThanOrEqualTo(expectedMin)), reason: 'Generated numbers unexpectedly include value(s) less than expected minimum'   );
            });
          });

          test('64-bit min', () {
            expectedMin  = expectedMin << 32;
            var callback = () => faker.randomGenerator.ints(expectedCount, min: expectedMin, max: expectedMax, only32bit: true);

            expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('value of `min` must be a 32-bit number'))), reason: 'Expected error not thrown');
          });
        });

        group('only64bit', () {
          group('32-bit min', () {
            test('32-bit max', () {
              var callback = () => faker.randomGenerator.ints(expectedCount, min: expectedMin, max: expectedMax, only64bit: true);

              expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('value of `max` must be a 64-bit number'))), reason: 'Expected error not thrown');
            });

            test('64-bit max', () {
              expectedMax = expectedMax << 32;
              var actual  = faker.randomGenerator.ints(expectedCount, min: expectedMin, max: expectedMax, only64bit: true);

              expect(actual, hasLength(equals(expectedCount)),                   reason: 'Generated list has unexpected number of elements'                             );
              expect(actual, everyElement(greaterThan(Int32.MAX_VALUE.toInt())), reason: 'Generated values unexpectedly included non-64-bit values'                     );
              expect(actual, everyElement(greaterThanOrEqualTo(expectedMin)),    reason: 'Generated numbers unexpectedly include value(s) less than expected minimum'   );
              expect(actual, everyElement(lessThanOrEqualTo(expectedMax)),       reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
            });
          });

          group('64-bit min', () {
            setUp(() {
              expectedMin  = expectedMin << 32;
            });

            test('32-bit max', () {
              var callback = () => faker.randomGenerator.ints(expectedCount, min: expectedMin, max: expectedMax, only64bit: true);

              expect(callback, throwsA(isA<ArgumentError>().having((x) => x.message, 'message', contains('value of `max` must be a 64-bit number'))), reason: 'Expected error not thrown');
            });

            test('64-bit max', () {
              expectedMax = expectedMax << 32;
              var actual  = faker.randomGenerator.ints(expectedCount, min: expectedMin, max: expectedMax, only64bit: true);

              expect(actual, hasLength(equals(expectedCount)),                   reason: 'Generated list has unexpected number of elements'                             );
              expect(actual, everyElement(greaterThan(Int32.MAX_VALUE.toInt())), reason: 'Generated values unexpectedly included non-64-bit values'                     );
              expect(actual, everyElement(greaterThanOrEqualTo(expectedMin)),    reason: 'Generated numbers unexpectedly include value(s) less than expected minimum'   );
              expect(actual, everyElement(lessThanOrEqualTo(expectedMax)),       reason: 'Generated numbers unexpectedly include value(s) greater than expected maximum');
            });
          });
        });
      });
    });

    group('decimals()', () {
      int    expectedCount = 0;
      double expectedMin   = 0;
      double expectedScale = 0;

      setUp(() {
        expectedCount = $orig.faker.randomGenerator.integer(1000, min: 10);
        expectedMin   = $orig.faker.randomGenerator.decimal(scale: 100,  min: 10         );
        expectedScale = $orig.faker.randomGenerator.decimal(scale: 100,  min: expectedMin);
      });

      test('basic', () {
        expectedMin   = 0; // -- native faker default value
        expectedScale = 1; // -- native faker default value
        var actual    = faker.randomGenerator.decimals(expectedCount);

        expect(actual, hasLength(equals(expectedCount)),                             reason: 'Generated list has unexpected number of elements'                   );
        expect(actual, everyElement(lessThanOrEqualTo(expectedScale + expectedMin)), reason: 'Generated list contained elements with values greater than expected');
        expect(actual, everyElement(greaterThanOrEqualTo(expectedMin)),              reason: 'Generated list contained elements with values less than expected'   );
      });

      test('with scale', () {
        expectedMin = 0; // -- native faker default value
        var actual  = faker.randomGenerator.decimals(expectedCount, scale: expectedScale);

        expect(actual, hasLength(equals(expectedCount)),                             reason: 'Generated list has unexpected number of elements'                   );
        expect(actual, everyElement(lessThanOrEqualTo(expectedScale + expectedMin)), reason: 'Generated list contained elements with values greater than expected');
        expect(actual, everyElement(greaterThanOrEqualTo(expectedMin)),              reason: 'Generated list contained elements with values less than expected'   );
      });

      test('with min', () {
        expectedScale = 1;   // -- native faker default value
        expectedMin   = 0.5; // -- something less than `scale`
        var actual    = faker.randomGenerator.decimals(expectedCount, min: expectedMin);

        expect(actual, hasLength(equals(expectedCount)),                             reason: 'Generated list has unexpected number of elements'                   );
        expect(actual, everyElement(lessThanOrEqualTo(expectedScale + expectedMin)), reason: 'Generated list contained elements with values greater than expected');
        expect(actual, everyElement(greaterThanOrEqualTo(expectedMin)),              reason: 'Generated list contained elements with values less than expected'   );
      });

      test('with scale & min', () {
        var actual    = faker.randomGenerator.decimals(expectedCount, scale: expectedScale, min: expectedMin);

        expect(actual, hasLength(equals(expectedCount)),                             reason: 'Generated list has unexpected number of elements'                   );
        expect(actual, everyElement(lessThanOrEqualTo(expectedScale + expectedMin)), reason: 'Generated list contained elements with values greater than expected');
        expect(actual, everyElement(greaterThanOrEqualTo(expectedMin)),              reason: 'Generated list contained elements with values less than expected'   );
      });
    });
  });

  group('list generators', () {
    group('elements()', () {
      List<String> source     = ['red', 'orange', 'yellow', 'green', 'blue', 'violet'];
      int          iterations = $math.pow(source.length, 3).toInt();

      List<List<String>> _generate(List<String> Function() callback) {
        List<List<String>> actuals = [];

        for(var i = 0; i < iterations; i++) {
          actuals.add(callback());
        }

        return actuals;
      }

      test('basic', () {
        var actuals = _generate(() => faker.randomGenerator.elements(source));

        expect(actuals, everyElement(hasLength(lessThanOrEqualTo(source.length))),                    reason: 'generated list unexpectedly has more elements than the source list'          );
        expect(actuals,   anyElement(hasLength(equals(source.length))),                               reason: 'generated list was never full-length, but should have been, at least once'   );
        expect(actuals,   anyElement(hasLength(equals(0))),                                           reason: 'generated list was never zero-length, but should have been, at least once'   );
        expect(actuals, everyElement(everyElement(predicate((element) => source.contains(element)))), reason: 'generated list unexpectedly contains elements not present in the source list');
      });

      group('with count', () {
        test('count <= source.length', () {
          for(var i = 0; i <= 1; i++) { // -- will test `count < source.length` and `count == source.length`
            var expectedCount = (source.length - 1) + i;
            var actuals       = _generate(() => faker.randomGenerator.elements(source, count: expectedCount));

            expect(actuals, everyElement(hasLength(equals(expectedCount))),                               reason: 'generated list unexpectedly has more/less elements than requested'           );
            expect(actuals, everyElement(everyElement(predicate((element) => source.contains(element)))), reason: 'generated list unexpectedly contains elements not present in the source list');
          }
        });

        test('count > source.length', () {
          var expectedCount = source.length*2;
          var actuals       = _generate(() => faker.randomGenerator.elements(source, count: expectedCount));

          expect(actuals, everyElement(hasLength(equals(expectedCount))),                               reason: 'generated list unexpectedly has more/less elements than requested'           );
          expect(actuals, everyElement(everyElement(predicate((element) => source.contains(element)))), reason: 'generated list unexpectedly contains elements not present in the source list');
        });

        group('with unique', () {
          test('count <= source.length', () {
            for(var i = 0; i <= 1; i++) { // -- will test `count < source.length` and `count == source.length`
              var expectedCount = (source.length - 1) + i;
              var actuals       = _generate(() => faker.randomGenerator.elements(source, count: expectedCount, unique: true));

              expect(actuals, everyElement(hasLength(equals(expectedCount))),                               reason: 'generated list unexpectedly has more elements than the source list'          );
              expect(actuals, everyElement(everyElement(predicate((element) => source.contains(element)))), reason: 'generated list unexpectedly contains elements not present in the source list');
              expect(actuals, everyElement(predicate((List list) => list.toSet().length == list.length)),   reason: 'generated list unexpectedly contains duplicate elements not present in the source list');
            }
          });

          test('count > source.length', () {
            var callback = () => faker.randomGenerator.elements(source, count: source.length+1, unique: true);

            expect(callback,
              throwsA(
                isA<ArgumentError>()
                .having((e) => e.message, 'message', contains('may not be greater than the number of source elements'))),
              reason: 'Expected error not thrown'
            );
          });
        });
      });

      test('with unique', () {
        var actuals = _generate(() => faker.randomGenerator.elements(source, unique: true));

        expect(actuals, everyElement(hasLength(lessThanOrEqualTo(source.length))),                    reason: 'generated list unexpectedly has more elements than the source list'                    );
        expect(actuals, everyElement(hasLength(greaterThanOrEqualTo(0))),                             reason: 'generated list unexpectedly has fewer elements than the source list'                   );
        expect(actuals, anyElement(hasLength(equals(source.length))),                                 reason: 'generated list was never max-length, but should have been, at least once'              );
        expect(actuals, anyElement(hasLength(equals(0))),                                             reason: 'generated list was never min-length, but should have been, at least once'              );
        expect(actuals, everyElement(everyElement(predicate((element) => source.contains(element)))), reason: 'generated list unexpectedly contains elements not present in the source list'          );
        expect(actuals, everyElement(predicate((List list) => list.toSet().length == list.length)),   reason: 'generated list unexpectedly contains duplicate elements not present in the source list');
      });
    });
  });

  group('string generators', () {
    group('strings()', () {
      int expectedCount     = 0;
      int expectedMinLength = 0;
      int expectedMaxLength = 0;

      setUp(() {
        expectedCount     = $orig.faker.randomGenerator.integer(1000, min: 10);
        expectedMinLength = $orig.faker.randomGenerator.integer(100,  min: 10);
        expectedMaxLength = $orig.faker.randomGenerator.integer(100,  min: expectedMinLength);
      });

      test('basic', () {
        var actual = faker.randomGenerator.strings(expectedCount, expectedMaxLength);

        expect(actual, hasLength(equals(expectedCount)),                              reason: 'Generated list has unexpected number of elements'                   );
        expect(actual, everyElement(hasLength(lessThanOrEqualTo(expectedMaxLength))), reason: 'Generated list contained elements with length greater than expected');
      });

      test('with min', () {
        var actual = faker.randomGenerator.strings(expectedCount, expectedMaxLength, minLength: expectedMinLength);

        expect(actual, hasLength(equals(expectedCount)),                                 reason: 'Generated list has unexpected number of elements'                   );
        expect(actual, everyElement(hasLength(lessThanOrEqualTo(expectedMaxLength))),    reason: 'Generated list contained elements with length greater than expected');
        expect(actual, everyElement(hasLength(greaterThanOrEqualTo(expectedMinLength))), reason: 'Generated list contained elements with length shorter than expected');
      });
    });

    group('internet generators', () {
      group('imageUrl()', () {
        test('basic', () {
          var actual = faker.internet.imageUrl(); // ignore: deprecated_member_use_from_same_package

          expect(actual, matches(RegExp(r'^http://[\w\.\-]+(/[\w\-]+)*/[\w\-]+\.\w{3}')), reason: 'Generated url has unexpected form');
        });

        test('with protocol', () {
          var expectedProtocol = $orig.faker.lorem.word();
          var actual = faker.internet.imageUrl(expectedProtocol); // ignore: deprecated_member_use_from_same_package

          expect(actual, matches(RegExp('^$expectedProtocol' + r'://[\w\.\-]+(/[\w\-]+)*/[\w\-]+\.\w{3}')), reason: 'Generated url has unexpected form'); // ignore: prefer_adjacent_string_concatenation
        });
      });
    });

    group('image generators', () {
      group('direct()', () {
        test('basic', () {
          var actual = faker.image.direct();

          expect(actual, matches(RegExp(r'^http://[\w\.\-]+(/[\w\-]+)*/[\w\-]+\.\w{3}')), reason: 'Generated url has unexpected form');
        });

        test('with protocol', () {
          var expectedProtocol = $orig.faker.lorem.word();
          var actual = faker.image.direct(protocol: expectedProtocol);

          expect(actual, matches(RegExp('^$expectedProtocol' + r'://[\w\.\-]+(/[\w\-]+)*/[\w\-]+\.\w{3}')), reason: 'Generated url has unexpected form'); // ignore: prefer_adjacent_string_concatenation
        });

        test('with query params', () {
          var expectedProtocol = $orig.faker.lorem.word();
          var expectedWidth    = $orig.faker.randomGenerator.integer(2000);
          var expectedHeight   = $orig.faker.randomGenerator.integer(2000);
          var expectedKeywords = List.generate($orig.faker.randomGenerator.integer(3, min: 1), (_) => $orig.faker.lorem.word())
            ..add([$orig.faker.lorem.word(), $orig.faker.lorem.word()].join(','));

          var actual = faker.image.direct(protocol: expectedProtocol, width: expectedWidth, height: expectedHeight, keywords: expectedKeywords, random: true);

          expect(actual, matches(RegExp('^$expectedProtocol' + r'://[\w\.\-]+(/[\w\-]+)*/[\w\-]+\.\w{3}\?' + 'width=$expectedWidth&height=$expectedHeight' + r'.+?&random=\d+$')), reason: 'Generated url has unexpected form'); // ignore: prefer_adjacent_string_concatenation
          var parsed = Uri.parse(actual);
          expect(parsed.queryParameters, containsPair('width',                    expectedWidth.toString()                                                           ), reason: 'Querystring value for `width` other than expected (or missing)' );
          expect(parsed.queryParameters, containsPair('height',                   expectedHeight.toString()                                                          ), reason: 'Querystring value for `height` other than expected (or missing)');
          expect(parsed.queryParameters, containsPair('random',                   isA<Object>().having((x) => int.tryParse(x.toString()), 'numeric value', isNotNull)), reason: 'Querystring value for `random` other than expected (or missing)');
          expect(parsed.queryParameters, containsPair(expectedKeywords.join(','), ''                                                                                 ), reason: 'Querystring keywords other than expected (or missing)'          );
        });
      });
    });

    group('guid generators', () {
      test('guids()', () {
        var expectedCount = $orig.faker.randomGenerator.integer(10, min: 2);
        var actual = faker.guid.guids(expectedCount);

        expect(actual, hasLength(expectedCount),                                  reason: 'Resultant list has unexpected length'     );
        expect(actual, everyElement(matches(RegExp(r'([a-f0-9]+\-)+[a-f0-9]+'))), reason: 'Resultant list contains unexpected values');
      });
    });

  });
}
