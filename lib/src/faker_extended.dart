import 'dart:core';
import 'dart:core' as $core;
import 'dart:math' as $math;
import 'package:faker/faker.dart';
import 'package:faker/src/image.dart'; // TODO: stop doing this once real faker starts exporting it like everything else
import 'package:fixnum/fixnum.dart';



extension RandomShort on Faker {
  RandomGenerator get random => randomGenerator;
}

extension RandomX on RandomGenerator {
  static final _rng = $math.Random();

  // ======== Number Generators

  /// generates a random integer whose value falls between [min] (inclusive) and [max] (exclusive)
  ///
  /// [only32bit] and [only64bit] will limit generated numbers to 32-bit or 64-bit integers (respectively).
  /// Setting both to true throws an [ArgumentError]
  /// Omitting both or setting both to false will randomly generate elements of both types
  /// If [only32bit] is set to true, then the value passed for [min] must be limited to a 32-bit max value; passing a number outside the 32-bit space will throw an [ArgumentError]
  $core.int int({$core.int min = 0, $core.int? max, bool only32bit = false, bool only64bit = false}) {
    return ints(1, max: max, min: min, only32bit: only32bit, only64bit: only64bit).first;
  }

  /// generates a list containing [count] random integers whose values fall between [min] (inclusive) and [max] (exclusive)
  ///
  /// [only32bit] and [only64bit] will limit generated numbers to 32-bit or 64-bit integers (respectively).
  /// Setting both to true throws an [ArgumentError]
  /// Omitting both or setting both to false will randomly generate elements of both types
  /// If [only32bit] is set to true, then the value passed for [min] must be limited to a 32-bit max value; passing a number outside the 32-bit space will throw an [ArgumentError]
  List<$core.int> ints($core.int count, {$core.int min = 0, $core.int? max, bool only32bit = false, bool only64bit = false}) {
    max ??= Int64.MAX_VALUE.toInt(); // -- force default even if `null` is explicitly passed

    if (only32bit && only64bit) {
      throw ArgumentError('Cannot set BOTH `only32bit` and `only64bit` to true');
    }

    $core.int gen32(i) => int32(max: $math.min(Int32.MAX_VALUE.toInt(), max!), min: min).toInt();
    $core.int gen64(i) => int64(max: max, min: $math.max((Int32.MAX_VALUE.toInt() + 1), min)).toInt();

    if (Int32.MAX_VALUE < min) {
      if (only32bit && !only64bit) { // -- requested only32 bit, but with a min value greater than 32-bit max
        throw ArgumentError('When `only32bit` is true, value of `min` must be a 32-bit number also');
      }

      only32bit = false;
      only64bit = true;
    }

    if (Int32.MAX_VALUE.toInt() >= max) {
      if (only64bit && !only32bit) {
        throw ArgumentError('When `only64bit` is true, value of `max` must be a 64-bit number also');
      }

      only32bit = true;
      only64bit = false;
    }

    $core.int Function($core.int i) callback;
    if (only32bit && !only64bit) { // -- xor
      callback = gen32;
    } else if (only64bit && !only32bit) { // -- xor
      callback = gen64;
    } else { // -- mixed
      callback = (i) => (this.boolean() ? gen32 : gen64)(i); // -- 50/50 chance
    }

    return List.generate(count, callback);
  }

  /// generates a random Int64 whose value falls between [min] (inclusive) and [max] (exclusive)
  Int64 int64({$core.int min = 0, $core.int? max}) {
    max ??= Int64.MAX_VALUE.toInt(); // -- force default even if `null` is explicitly passed

    if (min > max) {
      throw ArgumentError('Value passed for `min` ($min) must be less than value passed for `max` ($max)');
    }

    var multiplier = _rng.nextDouble();

    return Int64((multiplier * (max - min)).toInt() + min);
  }

  /// generates a list containing [count] random Int64s whose values fall between [min] (inclusive) and [max] (exclusive)
  List<Int64> int64s($core.int count, {$core.int min = 0, $core.int? max}) {
    max ??= Int64.MAX_VALUE.toInt(); // -- force default even if `null` is explicitly passed

    return List.generate(count, (i) => int64(min: min, max: max));
  }

  /// generates a random Int32 whose value falls between [min] (inclusive) and [max] (exclusive)
  Int32 int32({$core.int min = 0, $core.int? max}) {
    max ??= Int32.MAX_VALUE.toInt(); // -- force default even if `null` is explicitly passed

    if (min > Int32.MAX_VALUE.toInt()) {
      throw ArgumentError.value(min, 'min', 'must be a 32-bit number');
    }

    if (min > max) {
      throw ArgumentError('Value passed for `min` ($min) must be less than value passed for `max` ($max)');
    }

    return Int32(integer($math.min(Int32.MAX_VALUE.toInt(), max), min: $math.min(Int32.MAX_VALUE.toInt(), min)));
  }

  /// generates a list containing [count] random Int32s whose values fall between [min] (inclusive) and [max] (exclusive)
  /// if passed, the value of [min] must be limited to a 32-bit max value; passing a number outside the 32-bit space will throw an [ArgumentError]
  List<Int32> int32s($core.int count, {$core.int min = 0, $core.int? max}) {
    max ??= Int32.MAX_VALUE.toInt(); // -- force default even if `null` is explicitly passed

    if (Int32.MAX_VALUE < min) {
      throw ArgumentError('Value of `min` must be a 32-bit number');
    }

    return List.generate(count, (i) => int32(min: min, max: max));
  }

  /// generates a list containing [count] random doubles whose values fall between [min] (inclusive) and [scale]+[min] (inclusive)
  List<$core.double> decimals($core.int count, {$core.num scale = 1, $core.num min = 0}) {
    return List.generate(count, (_) => decimal(scale: scale, min: min));
  }

  // ======== List Generators

  /// Plucks a random subset of elements from the given [sourceElements]
  ///
  /// if [count] is passed, the resultant list will contain exactly [count] elements randomly chosen from the passed [sourceElements]
  /// if [count] is omitted, then a random count will be chosen in the range between 0 (inclusive) and [sourceElements.length] (exclusive)
  ///
  /// setting [unique] to true will ensure that the generated list will not contain the same source ELEMENT more than once.
  /// Note: if the source list contains the same VALUE multiple times, it's possible that each of those would be randomly selected into the returned list.
  List<T> elements<T>(List<T> sourceElements, {$core.int? count, bool unique = false}) {
    count  ??= integer(sourceElements.length + 1, min: 0); // -- force default even if `null` is explicitly passed

    if (unique) {
      if (count > sourceElements.length) {
        throw ArgumentError('When `unique` is true, the value passed for `count` ($count) may not be greater than the number of source elements (${sourceElements.length})');
      }

      sourceElements = List.from(sourceElements); // -- clone so we don't pop elements off the original
    }

    return List.generate(count, (i) {
      var index   = integer(sourceElements.length);
      T   element = (unique ? sourceElements.removeAt(index) : sourceElements[index]);

      return element;
    });
  }

  // ======== String Generators

  /// generates a list containing [count] random alphanumeric strings, each with length between [minLength] (inclusive) to [maxLength] (inclusive)
  List<$core.String> strings($core.int count, $core.int maxLength, {$core.int minLength = 1}) {
    return List.generate(count, (_) => string(maxLength, min: minLength));
  }
}

extension ImageUrls on Internet {
  @Deprecated('Since 0.0.6 - use `faker.image.direct()`')
  String imageUrl([String protocol = 'http']) => faker.image.direct(protocol: protocol);
}

extension DirectImageUrls on Image {
  String direct({
      String       protocol = 'http',
      int?         width,
      int?         height,
      List<String> keywords = const [],
      bool         random = false
  }) {
    var url = [
      faker.internet.uri(protocol),
        ...List.generate(faker.random.int(min: 1, max: 5), (_) => faker.lorem.word())
      ]
      .join('/') + '.' + faker.random.element(['jpg', 'png', 'gif', 'tif', 'bmp']);

    var queries = [];
    if (width  != null) queries.add('width=$width');
    if (height != null) queries.add('height=$height');
    if (keywords.isNotEmpty || random == true) {
      var superQuery = Uri.parse(faker.image.image(keywords: keywords, random: random)).query;
      queries.addAll(superQuery.split('&'));
    }

    if (queries.isNotEmpty) {
      url += '?' + queries.join('&');
    }

    return url;
  }
}

extension Guids on Guid {
  List<String> guids(int numberOfGuids) =>
    List<String>.generate(numberOfGuids, (_) => guid());
}
