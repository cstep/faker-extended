## 0.0.1

- Initial version

## 0.0.2

- fix pub.dev analysis warnings

## 0.0.3

- add `faker.random.strings()` and `faker.random.decimals()`

## 0.0.4

- add `faker.internet.imageUrl()` and `faker.guid.guids()`

## 0.0.5

- update all dependencies to most recent versions

## 0.1.0

- update to faker 2.0
- null safety support
