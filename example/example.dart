// ignore_for_file: unused_local_variable

import 'dart:math' as math;
import 'package:faker_extended/faker_extended.dart';
import 'package:fixnum/fixnum.dart';

void main() {
  Int32 single32a = faker.randomGenerator.int32(min: 100);                            // -- generate a single Int32 with a value of at least 100 and at most 2^32
  int   single32b = faker.randomGenerator.int(max: math.pow(2, 48).toInt(), only32bit: true); // -- generate a single int with a value of at least 100 and at most 2^32

  Int64 single64a = faker.randomGenerator.int64(max: math.pow(2, 48).toInt());                // -- generate a single Int64 with a value of at least 2^32 +1 and at most 2^64
  int   single64b = faker.randomGenerator.int(max: math.pow(2, 48).toInt(), only64bit: true); // -- generate a single int with a value of at least 2^32 +1 and at most 2^64

  int   singleAny = faker.randomGenerator.int(min: 100); // -- randomly generates either a 32-bit or 64-bit integer with a value of at least 100 and at most, either 2^32 or 2^64 (respectively)

  List<Int32> many32a = faker.randomGenerator.int32s(50, min: 100);                // -- generate a list containing 50 instances of Int32, each with a value of at least 100 and at most 2^32
  List<int>   many32b = faker.randomGenerator.ints(50, min: 100, only32bit: true); // -- generate a list containing 50 instances of int, each with a value of at least 100 and at most 2^32

  List<Int64> many64a = faker.randomGenerator.int64s(50, max: math.pow(2, 48).toInt());    // -- generate a list containing 50 instances of Int64, each with a value of at least 2^32 +1 and at most 2^64
  List<int>   many64b = faker.randomGenerator.ints(50, min: 100, only64bit: true); // -- generate a list containing 50 instances of int, each with a value of at least 2^32 +1 and at most 2^64

  List<int>   manyAny = faker.randomGenerator.ints(50, min: 100); // -- generate a list containing 50 instances of either a 32-bit or 64-bit integers, each with a value of at least 100 and at most, either 2^32 or 2^64 (respectively)
}
