![Bitbucket Pipelines branch](https://img.shields.io/bitbucket/pipelines/cstep/faker-extended/master?label=build)
![Codecov](https://img.shields.io/codecov/c/bitbucket/cstep/faker-extended/master?label=coverage)
![Dart Version](https://img.shields.io/badge/dart-%5E2.7-blue)
[![extra_pedantic on pub.dev](https://img.shields.io/badge/style-extra__pedantic-blue)](https://pub.dev/packages/extra_pedantic)
![License](https://img.shields.io/badge/license-Apache%202-blue)

[Faker](https://pub.dev/packages/faker) is a very useful library for Dart that generates fake data. [This package](https://pub.dev/packages/faker-extended) adds some additional generators to make life slightly easier.

## Usage

Use faker just like you always have. But wait! There's more!


```dart
import 'dart:math' as math;
import 'package:faker_extended/faker_extended.dart';
import 'package:fixnum/fixnum.dart';

void main() {
  Int32 single32a = faker.randomGenerator.int32(min: 100);                            // -- generate a single Int32 with a value of at least 100 and at most 2^32
  int   single32b = faker.randomGenerator.int(max: math.pow(2, 48), only32bit: true); // -- generate a single int with a value of at least 100 and at most 2^32

  Int64 single64a = faker.randomGenerator.int64(max: math.pow(2, 48));                // -- generate a single Int64 with a value of at least 2^32 +1 and at most 2^64
  int   single64b = faker.randomGenerator.int(max: math.pow(2, 48), only64bit: true); // -- generate a single int with a value of at least 2^32 +1 and at most 2^64

  int   singleAny = faker.randomGenerator.int(min: 100); // -- randomly generates either a 32-bit or 64-bit integer with a value of at least 100 and at most, either 2^32 or 2^64 (respectively)

  List<Int32> many32a = faker.randomGenerator.int32s(50, min: 100);                // -- generate a list containing 50 instances of Int32, each with a value of at least 100 and at most 2^32
  List<int>   many32b = faker.randomGenerator.ints(50, min: 100, only32bit: true); // -- generate a list containing 50 instances of int, each with a value of at least 100 and at most 2^32

  List<Int64> many64a = faker.randomGenerator.int64s(50, max: math.pow(2, 48));    // -- generate a list containing 50 instances of Int64, each with a value of at least 2^32 +1 and at most 2^64
  List<int>   many64b = faker.randomGenerator.ints(50, min: 100, only64bit: true); // -- generate a list containing 50 instances of int, each with a value of at least 2^32 +1 and at most 2^64

  List<int>   manyAny = faker.randomGenerator.ints(50, min: 100); // -- generate a list containing 50 instances of either a 32-bit or 64-bit integers, each with a value of at least 100 and at most, either 2^32 or 2^64 (respectively)
}

```

## Features and bugs

For now, please file feature requests and bugs by emailing the author
